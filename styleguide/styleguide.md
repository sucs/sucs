# Intro

This is a brief style guide for people creating SUCS content (posters, fliers, web design, etc).

# Name

The society name is Swansea University Computer Society (Welsh: *Cymdeithas Gyfrifruadurol Prifysgol Abertawe*), abbreviated to SUCS (*CGPA*).

Older documents may show the abbreviation as S.U.C.S., but this has not been in general usage for some time.

# Domains

Our main domain name is sucs.org
The domains sucs.swan.ac.uk, sucs.swansea.ac.uk and compsoc.swan.ac.uk all resolve to silver and will work for web access, but will be redirected to sucs.org

# Logos

There are 4 logos for SUCS, and all follow different purposes:

1. Primary - this logo is what you should use for most project, and includes the name of the society along with the icon.
2. Icon - this is the logo but missing the text. This is most useful for when you need a more compact logo, eg website icons, chat icons, etc.
3. Printing - these variations of the logo are missing the orange circle, are inverted, and are for printing SUCS materials onto white paper.
4. Legacy - these are the old logos, and will be deprecated over time, being replaced with the new logo. You shouldn't have any reason to use these in most cases, but we will keep them around just in case.

# Colors

* Primary Orange - #e86c39
* Secondary Orange - #DB863B
* Primary Dark Grey - #141414
* Secondary Dark Grey - #474747

# Fonts

1. Source Serif Pro - Formal
2. Source Code Pro - Code Stuff/Logo 
3. Source Sans Pro - General Text

# Usage

It's generally best to use Source Sans Pro for most text, and Serif Pro for more formal usage.

Use the primary logo in all cases you can, apart from semi-formal to formal printing, in which case use the printing logo. Only use the icons when necessary, e.g the text would be too small to read using the main logo, or for something like a Discord icon.

For printing etc, use pure white as the main colour. However, with digital projects, using Primary Dark Grey is the best choice, with Secondary Dark Grey as accenting. Introducing Primary Orange is essential, and best for interactive options. Use Secondary Orange for a "negative", and Primary Orange for a "positive". Eg, a join button - before joining, use Secondary Orange. After clicking join, switch it to primary orange.
