# SUCS Wifi

SUCS WiFi Network uses TTLS-PAP authentication.

Settings are:

```
Security: WPA2 enterprise
Authentication: TTLS / Tunneled TLS
CA Cert: No Ca cert required / do not validate
```

Leave all other fields blank and use your SUCS user/pass for the identity/password field.

# Linux
Note that iwd with NetworkManager as a frontend on Linux currently doesn't work with this connection method.
