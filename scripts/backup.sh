#!/bin/bash
REPOSITORY=backupuser@backup:/home/backups/
borg create -v --stats                                              \
    $REPOSITORY::`hostname`-`date +%Y-%m-%d-%R`                     \
    /home /etc /var /root                                           \
    --exclude '/home/*/noback/'                                     \
    --compression lzma,5

borg prune --stats --verbose $REPOSITORY --prefix `hostname`-       \
    --keep-daily=28                                                 \    
    --keep-weekly=8                                                 \
    --keep-monthly=24
